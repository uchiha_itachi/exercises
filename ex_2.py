s = input()
count_number = 0
count_letter = 0
for i in s:
    if(i.isdigit()):
        count_number += 1
    if(i.isalpha()):
        count_letter += 1

print("Number of digit: %d" % count_number)
print("Number of letter: %d" % count_letter)
