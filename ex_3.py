import pymongo
import base64
import requests
from requests.packages.urllib3.exceptions import InsecureRequestWarning

requests.packages.urllib3.disable_warnings(InsecureRequestWarning)


url = "https://github.com/"

myclient = pymongo.MongoClient("mongodb://localhost:27017/")
mydb = myclient["Nguyen_Van_An"]
mycol = mydb["list_url"]


def encode_url(url):

    res = requests.get(url, verify=False, timeout=10)
    data_str = res.text
    data_bytes = data_str.encode("utf-8")
    b64_encoded = base64.b64encode(data_bytes)
    return b64_encoded


def insert_to_db(url, content_encoded):
    url_to_insert = {
        "url": url,
        "content": content_encoded
    }
    mycol.insert_one(url_to_insert)


def decode_url_content(url):
    decode_url_content = None
    for x in mycol.find():
        if url == x["url"]:
            decode_url_content = base64.b64decode(x["content"])
    if(decode_url_content is None):
        return ("This page is not in DB")
    else:
        return str(decode_url_content, "utf-8")


content_encoded = encode_url(url)
# insert_to_db(url, content_encoded)
print(decode_url_content(url))
